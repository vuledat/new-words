package newWords;

/**
 *
 * @author JAMES
 */
public class Word {
    String en, vi, type, pronounce;
    Integer id, isLearned;

    public Word(String en, String vi, String type, Integer id, Integer isLearned) {
        this.en = en;
        this.vi = vi;
        this.type = type;
        this.id = id;
        this.isLearned = isLearned;
    }
    
    public Word(String en, String vi, String type, Integer isLearned) {
        this.en = en;
        this.vi = vi;
        this.type = type;
        this.isLearned = isLearned;
    }

    public Word(String en, String vi, String type, String pronounce, Integer id, Integer isLearned) {
        this.en = en;
        this.vi = vi;
        this.type = type;
        this.pronounce = pronounce;
        this.id = id;
        this.isLearned = isLearned;
    }

    public String getPronounce() {
        return pronounce;
    }

    public void setPronounce(String pronounce) {
        this.pronounce = pronounce;
    }

    public String getEn() {
        return en;
    }

    public void setEn(String en) {
        this.en = en;
    }

    public String getVi() {
        return vi;
    }

    public void setVi(String vi) {
        this.vi = vi;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIsLearned() {
        return isLearned;
    }

    public void setIsLearned(Integer isLearned) {
        this.isLearned = isLearned;
    }

    
    
}
